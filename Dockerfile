FROM rabbitmq:3.12.2-management-alpine

RUN cd /opt/rabbitmq/plugins && wget https://github.com/noxdafox/rabbitmq-message-deduplication/releases/download/0.6.1/rabbitmq_message_deduplication-0.6.1.ez && wget https://github.com/noxdafox/rabbitmq-message-deduplication/releases/download/0.6.1/elixir-1.13.4.ez


RUN rabbitmq-plugins enable --offline rabbitmq_message_deduplication
